resource "aws_eip" "ip-aws" {
  domain = "vpc"
  tags = {
    Name = "${var.workspace_id}-eip"
  }
}
