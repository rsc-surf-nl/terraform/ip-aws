output "id" {
  value = aws_eip.ip-aws.id
}

output "ip_id" {
  value = aws_eip.ip-aws.id
}

output "address" {
  value = aws_eip.ip-aws.public_ip
}

output "description" {
  value = var.description
}
